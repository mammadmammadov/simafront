import Report from '../src/Pages/Report/Report'
import TableComponent from './Components/TableComponent';
import db from './db.json'

function App() {
  return (
    <div className="App">
      <TableComponent data={db.comments} withLink={true}></TableComponent>
    </div>
  );
}

export default App;
