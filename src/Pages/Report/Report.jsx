import React from "react";
import axios from "axios";
import { useEffect, useState } from "react";
import TableComponent from "../../Components/TableComponent";
 function Report() {
  const [data, setData] = useState([]);
  
  useEffect(() => {
    getComments();
  }, []);

  function getComments() {
    var aaa = axios.get("http://localhost:3000/posts").then((res) => setData(res?.data));
  }

  return (
    <div>
      <TableComponent data={data} simpleData={true}></TableComponent>
    </div>
  );
}

export default Report;