import React from "react";
import { Table } from "reactstrap";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


function TableComponent({
  data,
  simpleData,
  withDelete,
  withLink,
  excluded,
  ...props
}) {

  function displayHeader(data) {
    return (
      <thead>
        <tr>
          {Object.keys(data?.[0]).map((i) => (
            <th scope="row">{i}</th>
          ))}
          {withLink&& (<th scope="row"><a href="#" style={{ color: "blue" }}>Link</a></th> )}
        </tr>
      </thead>
    );
  }

  function displayData(data) {
    return (
      <tbody>
        {data.map((i) => (
          <tr>
            {Object.values(i).map((k) => (
              <td>{k}</td>
            ))}
           
          </tr>
        ))}
      </tbody>
    );
  }

  function displayDataWithDeleteButton(data) {
    return data.map((i) => (
      <tr>
        {Object.values(i).map((k) => (
          <td>{k}</td>
        ))}
         {withDelete&& (<td onClick={(e)=>{
          console.log(i);
         }} > <FontAwesomeIcon icon={faTrash} /></td> )}
      </tr>
    ));
  }

  function displayDataWithLink(data) {
    return data.map((i) => (
      <tr>
        {Object.values(i).map((k) => (
          <td>{k}</td>
        ))}
        <td>
          <a href="#" style={{ color: "blue" }}>Link</a>{" "}
        </td>
      </tr>
    ));
  }

  return (
    <Table style={{}}>
      {displayHeader(data)}
      {simpleData && displayData(data)}
      {withDelete && displayDataWithDeleteButton(data)}
      {withLink && displayDataWithLink(data)}
    </Table>
  );
}

export default TableComponent;
